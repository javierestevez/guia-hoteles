$(function () {
		  $('[data-toggle="tooltip"]').tooltip();
		  $('[data-toggle="popover"]').popover();
		  $('.carousel').carousel({
		  	interval: 5000
		  });

		  $('#reservarModal').on('show.bs.modal', function(e) {

		  	console.log('se abrió el formulario modal');
		  	$('#contactoBtn').prop('disabled', true);
		  	$('#contactoBtn').removeClass('btn-success');
		  	$('#contactoBtn').addClass('btn-danger');

		  });
		  $('#reservarModal').on('shown.bs.modal', function(e) {
		  	console.log('Fue mostrado el formulario modal');
		  });
		  $('#reservarModal').on('hide.bs.modal', function(e) {
		  	console.log('Se ocultó el formulario modal');
		  });
		  $('#reservarModal').on('hidden.bs.modal', function(e) {

		  	console.log('Fue ocultado el formulario modal');
		  	$('#contactoBtn').prop('disabled', false);
		  	$('#contactoBtn').removeClass('btn-danger');
		  	$('#contactoBtn').addClass('btn-success');
		  });
		});